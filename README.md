## Introduction

This is a eCommerce project written in Laravel 5.* intended for building a friendly eStore either for startups or big companies, and is highly opinionated towards that use case. Because it is a platform already built, you only will need to add as many products as you need  to start selling and taking orders from customers

## Installation

First, clone this repository into your local folder

Next, you will need to run composer update/install into your project folder
```
composer update/install
```

After run composer update, be sure to create and migrate your database and define your environment variables in your `.env` file. Also you have to run artisan migrate command, in arden to create the Antvel schema.

```
php artisan migrate:refresh
```

Then, you must run the Antvel seeder to create a functional demo, like so:

```
php artisan migrate:refresh --seed
```

At this point we are just missing run ***bower install***, in order to have all the frontend dependencies in your machine.  So, go to your directory using command line, as so:

```
bower install
```

After run all these commands you should be able to look at your version running in your browser without problems.

## Using Laravel Elixir

First at all, you need to install Gulp package typing this command line in your terminal. Like so:

```
npm install --global gulp
```

Second at all, you will need to step into your folder, like so:
```
cd app_folder
```

Third at all, type this following command in order for you to install the npm dependencies, like so:
```
npm install
```

If you are developing on Windows or running your VM on it, you must try like so:
```
npm install --no-bin-links
```

Now feel free to run gulp into your folder, like so:
```
gulp
```


## reCaptcha Settings

Now tha you have everything setted up, you need to set the reCaptcha variables, in order to have the user loging working in that mode. First you need to go to the reCaptcha web site and get your environments values

```
then you have to fill out the "Register a new site" form in order to obtain the reCaptcha keys

```
RECAPTCHA_PUBLIC_KEY, and RECAPTCHA_PRIVATE_KEY
```

So, you will need to assign those values into ```.env``` file. For example:

```
RECAPTCHA_PUBLIC_KEY = someRecaptcahPrivateKey

RECAPTCHA_PRIVATE_KEY = someRecaptcahPrivateKey
```

***Note:*** If the ```APP_DEBUG == true```, the reCaptcha will not be applied

## Features

* Responsivity
* Open Source
* Social Media Integration
* Unlimited Categories
* Unlimited Products
* Unlimited Manufacturers
* Your Own Style
* Multi Language
* Multi Currency
* Product Reviews
* Product Ratings
* Downloadable Products
* Automatic Image Resizing
* Multiple Tax Rates
* Related Products (What other customers are looking at, Recommendations for you in our categories, Store Trending)
* Search Engine Optimization
* Sales Reports
* Wish Lists
* Products Suggestions
* Products Grouping
* Addresses Book
* User Profiles (Sellers & Buyers)
* Products barcode
* Administrative Panel
* Users Orders list with action status (process, Placed, Cancel, Rated)
* Virtual Products (products key download integrated)
* Free Products Module
* Users Points
* Virtual Products Delivery
* Dynamic Products Features
* Company Profile
* Shopping Cart
* Users preferences control
* Users Notifications
* Company Info CMS
* Search Engine Suggestions
* Dynamic Breadcrumbs
